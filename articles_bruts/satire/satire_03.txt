Trump’s Agreeing to Talk to Woodward Shows Downside of Never Having Read a Book in Entire Life

WASHINGTON (The Borowitz Report)—Donald J. Trump’s decision to talk to Bob Woodward demonstrates the downside of never having read a book in his entire life, experts say.

While millions of Americans were astonished that Trump would voluntarily speak at great length to an author famous for his takedowns of Presidents, experts believe that a total obliviousness to books and what is inside them might have played a pivotal role.

Davis Logsdon, a University of Minnesota professor who studies the psychology of people who have never read a book in their lives, said that such people might be overconfident about how they would be portrayed if a book were ever written about them.

“If you’ve never read a book in your life, you might be under the impression that all books are flattering,” he said. “You would have no idea that a book could portray you as a human dumpster fire.”

As for Trump, Logsdon said that the President would “definitely benefit” from reading a book someday, but added, “It’s a little late for that now.”