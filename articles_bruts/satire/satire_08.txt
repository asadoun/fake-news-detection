I’m Not a Terrible Person Because I’m a Cop, I’m a Terrible Person That Became a Cop

As a 20 year veteran of the police force, I am getting real tired of everyone bashing the shield. Somehow wearing this uniform every day makes me the bad guy. All of a sudden everyone thinks we’re all monsters because of our careers. It’s all a bunch of bullshit. I was a terrible person long before I became a cop.

People want to say that the world of law enforcement creates a bully mentality. Ha! If they think that, they should contact all the nerds I shoved to the ground and called homophobic slurs in middle school. They’ll tell you straight up I don’t need a gun or a badge to be a bully.

I read an article recently that claimed first responders are encouraged to “dissociate tragedy” to be able to do their jobs properly, and this leaves a lot of us incapable of feeling emotion in any circumstance. What a bunch of touchy-feely mumbo jumbo. Ask any of the guys from my dogfighting league how many tears I shed when something terrible happens. I’ve been fighting dogs my entire life. Not organizing dog fights, mind you. I’ve been fist fighting dogs since I was 8 and I’m not gonna stop now that I finally have a winning record.

And my favorite stereotype is that being on the force magically makes us racist. Like they taught us how to be racist at the academy. Let me make this perfectly clear: I was teaching people how to be racist at the academy. Don’t give the police that much credit when it comes to changing hearts and minds.

The police force didn’t make me racist. I’ve been profiling people of color since I learned colors. That natural instinct to be an outright bigot can’t be taught.

All these liberal ANTIFA types think they have it all figured out. They think all they have to do is reform the police and suddenly I won’t be shooting unarmed people on the street. This is more than a job to me. Being an unrelenting piece of shit is my lifestyle. I cherish the ability to oppress and brutalize without accountability. Just ask my wife, she’ll tell you.