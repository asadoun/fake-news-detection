White House Gives Mixed Signals on Trump's Health

WASHINGTON—People close to President Trump offered conflicting messages about his health on Saturday after he tested positive for coronavirus, as his physician said he was happy with the U.S. leader’s condition while a person familiar with Mr. Trump’s health said there was more cause for concern.

In a briefing in front of the Walter Reed military hospital Saturday, Dr. Sean Conley said the president’s symptoms of a mild cough, nasal congestion and fatigue were improving and that he hadn’t had a fever for 24 hours. But he didn’t give a date for the president’s release and declined to provide a definitive answer on whether Mr. Trump had ever received supplemental oxygen, despite repeated pressing.

After Dr. Conley concluded his briefing, a person familiar with the president’s health told reporters that Mr. Trump’s vitals over the last 24 hours were very concerning and that the next 48 hours would be critical. “We’re still not on a clear path to a full recovery,” the person said.

The White House didn’t immediately respond to requests for clarification on the president’s condition.

From the hospital Saturday, Mr. Trump thanked the medical personnel at Walter Reed, adding: “With their help, I am feeling well!”

Dr. Conley declined to give a date for when Mr. Trump last tested negative, as questions persisted about when the White House learned of the president’s exposure and subsequent positive test. Dr. Conley also referred to the president as being 72 hours into his diagnosis, which would put the date of his positive test on Wednesday. A White House official subsequently said Dr. Conley meant to say it was the third day since the president was diagnosed Thursday night, not that 72 hours had passed since his diagnosis.

In a memo hours after his briefing concluded on Saturday, Dr. Conley said the president had been diagnosed on the evening of Oct. 1, which was Thursday.

On whether the president had ever received oxygen, Dr. Conley said: “Thursday no oxygen, none at this moment, and yesterday with the team while we were all here, he was not on oxygen.”

Mr. Trump was hospitalized on Friday following a positive Covid-19 test early that day for him and first lady Melania Trump, hours after senior White House adviser Hope Hicks was reported to test positive.

The White House said the move was made out of an “abundance of caution. But it raised questions about his health, his government and his re-election bid one month before election day.

As the president was treated, a cascading list of aides and allies revealed positive test results. Many of them were among the attendees of a Rose Garden ceremony last Saturday where Mr. Trump announced his Supreme Court nominee, which featured close seating and little mask wearing.

The list included former White House adviser Kellyanne Conway, Republican Sens. Mike Lee of Utah and Thom Tillis of North Carolina and John Jenkins, the president of the University of Notre Dame.

The president’s campaign manager Bill Stepien, who was with the president for debate preparation last weekend and at the first presidential debate in Cleveland, also tested positive, as did former New Jersey Gov. Chris Christie, who was involved in debate preparation. Other White House and campaign aides reported negative tests, and some were awaiting results.

The revelation of Mr. Trump’s infection put campaign travel on hold and prompted senior aides to get tests and track down people who may have been exposed to the virus.

Vice President Mike Pence has tested negative twice in the last two days and hasn’t quarantined on the advice of his doctor. The Trump campaign said Saturday that the vice president would hold a campaign event in Arizona on Thursday.

Late Friday evening, Dr. Conley, said in a memo that the president had completed his first dose of remdesivir, which is among the few drugs that have been shown to treat Covid-19 and to have been cleared by the Food and Drug Administration for such use.

Mr. Stepien was experiencing mild symptoms and plans to quarantine until he recovers, the campaign said. Republican National Committee chairwoman Ronna McDaniel also tested positive earlier this week, the GOP said.

As Mr. Trump canceled trips to Florida and Wisconsin, it remained unclear when—or if—the president would be able to return to the campaign trail and whether he would be able to participate in the remaining two debates with his Democratic rival Joe Biden. Mr. Biden on Friday tested negative for the virus, his campaign said.

Mr. Trump’s positive test results underscored the unrelenting nature of the virus as Americans grapple with the trade-offs between reopening businesses and schools and staying safe. The diagnoses exposed severe holes in the White House’s Covid-19 protocol, which has largely consisted of frequent rapid tests. Health experts say that conducting frequent tests without also wearing masks and social distancing is an ineffective way of combating the virus, since it can take days for a person to test positive after being infected.

The White House will continue to make mask-wearing optional on its grounds, an official said Friday.

White House officials said the president’s diagnosis wouldn’t affect his governing. White House communications director Alyssa Farah said there would be no transfer of power with the president’s move to Walter Reed.

Still, Mr. Trump asked Mr. Pence to host a scheduled call in his place on Friday afternoon.

The news that the president was ill rattled markets and sent a shock wave through Washington, which is grappling with a Supreme Court nomination, trillion-dollar-plus coronavirus aid talks and an election that will decide control of the White House and Congress just a month away. It also creates potential uncertainty for his Supreme Court nominee, Judge Amy Coney Barrett, whom Republicans are trying to get confirmed by Election Day.

Democrats called for postponing Judge Barrett’s confirmation, arguing that the infection of two senators could make it difficult to hold in-person hearings. Senate Majority Leader Mitch McConnell (R., Ky.) and Sen. Lindsey Graham (R., S.C.), who both spoke with Mr. Trump on Friday, said they plan to move forward.

Senior officials were at the White House on Friday, and it wasn’t known how many aides who had been in contact with the president this week were quarantining. Centers for Disease Control and Prevention guidelines call for individuals to quarantine for 14 days after coming in close contact with someone who has tested positive for the coronavirus.

Mr. Trump traveled each of the three days leading up to his diagnosis, with dozens of White House staffers, top campaign aides, senior Republican Party officials, and at least five members of Congress, according to White House travel logs reviewed by The Wall Street Journal.

White House chief of staff Mark Meadows on Friday confirmed that the White House was aware Ms. Hicks had tested positive before the president left for New Jersey on Thursday, where he attended two events with supporters—one inside, one outside. Mr. Meadows said the White House decided to pull some aides from the trip after learning of Ms. Hicks’s test results.

Ms. Hicks traveled with the president twice this week, including to Minnesota for a campaign rally on Wednesday, when she began to feel symptoms and quarantined for part of the return flight.

At the White House on Friday, press secretary Kayleigh McEnany told reporters that officials had cleared Mr. Trump’s trip to New Jersey as safe. New Jersey Gov. Phil Murphy urged anyone who had been in or around the Bedminster, N.J., Trump event to quarantine and get tested.

While many Covid-19 patients recover without suffering serious illness, Mr. Trump’s age of 74 puts him at higher risk from the virus, as does his obesity. According to the CDC, people in their 60s and 70s are “at higher risk for severe illness than people in their 50s.”

If the president’s condition were to worsen, he could temporarily transfer power to Mr. Pence under the 25th Amendment. Such a transfer has happened only three times in U.S. history, when then-presidents Ronald Reagan and George W. Bush underwent colonoscopies. In other incidents, such as when Mr. Reagan was shot and had to undergo emergency surgery in 1981, power wasn’t formally transferred.