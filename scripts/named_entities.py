# -*- coding: utf-8 -*-
"""
Created on Mon Jan 11 22:55:22 2021

@author: asadoun
"""
import sys  
import nltk
import pandas as pd
from nltk.corpus import stopwords  
from nltk.tokenize import word_tokenize 
#reload(sys)  
#sys.setdefaultencoding('utf8') 
#nltk.download('maxent_ne_chunker')
#nltk.download('words')

file=open('C:\\Users\\User\\true_01.txt');
text=file.read();

words = word_tokenize(text)
postag = nltk.pos_tag(words)
chunks = nltk.ne_chunk(postag, binary=False)
entities = []
labels = []
for chunk in chunks:
    if hasattr(chunk, 'label'):
        #print(chunk)
        entities.append(''.join(c[0] for c in chunk))
        labels.append(chunk.label())
        
entities_labels = list(set(zip(entities, labels)))
entities_df = pd.DataFrame(entities_labels)
entities_df.columns = ["Entities", "Labels"]
print(entities_df)
appendFile = open('C:\\Users\\User\\true_1_named_entities.txt','a') 
appendFile.write(entities_df.to_string()) 
appendFile.close() 
