# -*- coding: utf-8 -*-
"""
Created on Mon Jan 18 01:50:00 2021

@author: asadoun
"""
import collections
import re 
cnt = collections.Counter()
wanted = set(['accordingly', 'as a consequence', 'as a result', 'consequently', 'due to', 'for this reason', 'forthwith', 'hence', 'then', 'therefore', 'thereupon', 'thus', 'above all', 'absolutely', 'chiefly', 'clearly', 'definitely', 'especially', 'even', 'importantly', 'in detail', 'in truth', 'indeed', 'it should be noted', 'naturally', 'never', 'obviously', 'of course', 'particularly', 'in particular', 'positively', 'surprisingly', 'to clarify', 'to emphasize', 'to repeat', 'truly', 'undoubtedly', 'unquestionably', 'with attention', 'without a doubt', 'additionally', 'an additional', 'along with', 'also', 'and', 'apart from this', 'as well as', 'as well as that', 'besides', 'coupled with', 'finally', 'first', 'further', 'furthermore', 'in addition', 'in addition to this', 'in the same fashion', 'last', 'moreover', 'not only ', 'not to mention', 'second', 'similarily', 'third', 'together with', 'too', 'because of', 'for the purpose of', 'given that', 'granted that', 'in fact', 'in order to', 'in view of', 'owing to', 'provided that', 'seeing that', 'so that', 'with this in mind', 'with this intention', 'with this purpose', 'as an example of', 'for example', 'for instance', 'for one thing', 'illustrated by ', 'in another case', 'in the case of', 'in this case', 'in this situation', 'including', 'like', 'namely', 'on this occasion', 'proof of this', 'specifically', 'such as', 'to clarify', 'to demonstrate', 'to clarify', 'to simplify', 'alternatively', 'as opposed to', 'contrarily', 'contrary to', 'conversely', 'despite/in spite of', 'differing from', 'even so', 'however', 'in contrast ', 'in contrast to', 'in opposition', 'instead', 'nevertheless', 'nonetheless', 'nor', 'notwithstanding', 'on the other hand', 'rather', 'though', 'unlike', 'up against', 'whereas', 'while', 'yet', 'alike', 'as with', 'both', 'by the same token', 'compare', 'compared to', 'compared with', 'correspondingly', 'either', 'equal', 'equally', 'in a similar manner', 'in common', 'in like manner', 'in the same way', 'in the spitting image of', 'just as', 'just as…so too', 'just like', 'likewise', 'most important', 'resembles', 'same as', 'similar to', 'similarly', 'still another', 'first', 'firstly', 'second', 'secondly', 'third', 'thirdly', 'finally', 'at this time', 'following', 'previously', 'before', 'subsequently', 'above all', 'lastly and most importantly', 'last but not least', 'first and foremost', 'all things considered', 'altogether ', 'as demonstrated above', 'as noted', 'as shown above', 'as you can see', 'briefly', 'by and large', 'generally speaking', 'given these points', 'in a word', 'in any event', 'in brief', 'in conclusion', 'in essence', 'in short', 'in summary', 'in the end', 'in the final analysis', 'on the whole', 'overall', 'therefore', 'to conclude', 'to end', 'to sum up', 'to summarise', 'ultimately', 'although this may be true', 'as', 'because of', 'even if', 'given that', 'granted that', 'if', 'in that case', 'in the event that', 'lest', 'on the condition that', 'only if', 'since', 'then', 'unless', 'when', 'whenever', 'while', 'admittedly', 'albeit', 'all the same', 'although', 'although/even though', 'and still', 'and yet', 'be that as it may', 'even if', 'even so', 'even though', 'however', 'in spite of', 'nevertheless', 'nonetheless', 'regardless of this', 'up to a point', 'as a rule', 'broadly speaking', 'commonly', 'for the most part', 'generally speaking', 'in general/ generally', 'in most cases', 'mainly', 'more often than not', 'mostly', 'normally', 'often', 'on the whole', 'overall', 'predominately', 'regularly', 'typically', 'alternatively', 'alternatively stated', 'expressed simply', 'in a nutshell', 'in other words', 'in short', 'in simple language', 'in simple terms', 'in summation', 'namely', 'otherwise stated', 'put differently', 'put in another way ', 'reiterated', 'said differently', 'simplified', 'simply put', 'that is to say', 'to put it differently', 'as applied to', 'as far as', 'concerning', 'considering', 'in connection to', 'in terms', 'pertaining to', 'regarding', 'some examples of these might be:', 'speaking about/of', 'the fact that', 'with regards to', 'with respect to', 'i mean', 'in explanation', 'in lay terms', 'in other words', 'in simple terms', 'simply put', 'simply stated', 'that is to say', 'to break it down', 'to clearly define', 'to explain', 'to make plain', 'to put it clearly', 'to put it in another way', 'to simplify', 'across', 'adjacent', 'adjacent', 'around', 'at the rear', 'below', 'beneath', 'near', 'nearby', 'next to', 'on bottom', 'on top', 'opposite', 'over', 'surrounding', 'to the left', 'underneath'])
words = re.findall('\w+', open('C:\\Users\\User\\Desktop\\articles\\articles_brut\\satire\\satire_combined.txt', encoding='utf-8').read().lower())

for word in words:
    if word in wanted:
        cnt [word] += 1
for w1, w2 in zip(words, words[1:]):
    phrase = w1 + " " + w2
    if phrase in wanted:
        cnt[phrase] += 1
for w1, w2, w3 in zip(words, words[1:], words[2:]):
    phrase2 = w1 + " " + w2 + " " + w3
    if phrase2 in wanted:
        cnt[phrase2] += 1
for w1, w2, w3, w4 in zip(words, words[1:], words[2:], words[3:]):
    phrase3 = w1 + " " + w2 + " " + w3 + " " + w4
    if phrase3 in wanted:
        cnt[phrase3] += 1
for w1, w2, w3, w4, w5 in zip(words, words[1:], words[2:], words[3:], words[4:]):
    phrase4= w1 + " " + w2 + " " + w3 + " " + w4 + " " + w5
    if phrase4 in wanted:
        cnt[phrase4] += 1
print(cnt)