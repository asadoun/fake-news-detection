#Mémoire de master - Université de Strasbourg - Master technologies des langues

Noms de dossiers :
```
📂 articles_bruts : articles collectés et non traités
    └─── 📂 fake   : articles de désinformation
    └─── 📂 satire : articles satiriques
    └─── 📂 true   : articles vrais
```

```
 📂 articles_nettoyes : articles nettoyés
    └─── 📂 fake   : articles de désinformation nettoyés
    └─── 📂 satire : articles satiriques nettoyés
    └─── 📂 true   : articles vrais nettoyés
    └─── 📂 fusion_exp   : articles fusionnés (pouvant servir pour parcourir les corpus plus facilement)

     🟩 articles_clean_stats.xlsx : fichier de statistiques sur les articles nettoyés
     🟩 articles_clean_stats_with_NUMBR.xlsx : fichier de statistiques avec le label NUMBR comme fusion des tokens numériques
```

```
📂 cooccurrences : données sur les co-occurrences 
     📄 fake_cooccurrences_2_words.txt    : couples de mots du corpus « fake » les plus fréquents
     📄 satire_cooccurrences_2_words.txt  : couples de mots du corpus « satire » les plus fréquents
     📄 true_cooccurrences_2_words.txt    : couples de mots du corpus « true » les plus fréquents
     🟩 co-occurrences compare.xlsx       : tableaux contenant les co-occurrences les plus fréquentes
```

```
📂 linking_words : données sur les connecteurs discursifs (linking words)
    └─── 📂 txt files : fichiers statistiques générés par l'algorithme Python sur

      🟩 linking_words.xlsx     : tableaux sur les connecteurs selon la fréquence
      📄 linking_words_list.txt : liste des connecteurs discursifs utilisée par l'algorithme
```

```
📂 named_entities : entités nommées
     📄 fake_name_entities_fixed.txt     : fichier traité détaillant les entités nommées détectées du corpus « fake »
     📄 satire_name_entities_fixed.txt   : fichier traité détaillant les entités nommées détectées du corpus « satire »
     📄 true_name_entities_fixed.txt     : fichier traité détaillant les entités nommées détectées du corpus « true »

     🟩 named_entities_stats.xlsx        : feuille de statistiques
```

```
📂 other : autres fichiers utilisés
     📄 stopwords.txt  : fichier contenant la liste de stopwords utilisée 
     
```

```
📂 scripts : scripts python utilisés
     🔹 count_linking_words.py            : script comptant les connecteurs discursifs
     🔹 named_entities.py                 : script identifiant les entités nommées
     🔹 sentiment_analysis.py             : script TextBlob pour l'analyse de sentiments
     🔹 stopwords.py                      : script de nettoyage de texte (stopwords)

```

```
📂 talismane : résultats de l'analyse via Talismane
     📄/🟩 fake_combined.txt/xlsx   : corpus « fake » 
     📄/🟩 satire_combined.txt/xlsx : corpus « satire »
     📄/🟩 true_combined.txt/xlsx   : corpus « true »   
```

```
📂 textometry : données d'analyse textométrique
    🟩 top 300 words.xlsx   :  300 mots les plus fréquents 
```

```
📂 treetagger : données d'analyse via TreeTagger
    └─── 📂 tag_fake   : corpus « fake »  
    └─── 📂 tag_satire : corpus « satire »
    └─── 📂 tag_true   : corpus « true »   
```
