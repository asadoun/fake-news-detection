GOP lawmaker who make racial and sexual comment step down from leadership post 

 MADISON - a top Assembly Republican who make racial and sexual remark to female colleague be step down from -PRON- leadership post but be refuse to leave the state Legislature despite a call to do so from Gov. Scott Walker . 

 Rep. Rob Brooks ( R - Saukville ) resign from -PRON- position as assistant majority leader Wednesday , hour after the Milwaukee Journal Sentinel report offensive remark -PRON- make to three female lawmaker over the summer , prompt an internal review . 

 Brooks ' self - demotion come after Walker call for Brooks to leave office . 

 " Representative ( Rob ) Brooks ' comment be offensive and disrespectful . -PRON- have no place in -PRON- society and be inconsistent with the high standard that must be hold by those in public office . -PRON- should resign from office , period , " Walker tweet . 

 Assembly Speaker Robin Vos ( R - Rochester ) and Majority Leader Jim Steineke ( R - Kaukauna ) issue a statement Wednesday follow Walker 's call , say the Assembly GOP caucus member " condemn the offensive remark " and say the " issue have be appropriately resolve " and suggest -PRON- do not call for Brooks to leave -PRON- leadership post . 

 meanwhile , the three female lawmaker subject to the remark say Brooks ’ decision to step down from -PRON- leadership role be " beyond what -PRON- expect or even want . " 

 " Rep. Brooks ' comment be out of line in July and -PRON- make -PRON- view immediately know to -PRON- and the Assembly chief clerk , " state Republican Reps . Cindi Duchow of Delafield , Amy Loudenbeck of Clinton and Jessie Rodriguez of Oak Creek say in a statement . " -PRON- be satisfied with the Assembly response this summer and have fully accept Rep. Brooks ' apology . while -PRON- respect -PRON- decision to resign -PRON- leadership position and appreciate the seriousness with which -PRON- take the issue , -PRON- be beyond what -PRON- expect or even want . -PRON- have put the incident behind -PRON- ; -PRON- would ... kindly ask other to do the same . " 

 Brooks , who have a $ 500-per - person fundraiser on Thursday , do not respond to phone call and text message on Wednesday . 

 in July , at a restaurant after an Assembly GOP caucus in Wisconsin Dells , Brooks say -PRON- would buy drink for everyone but Rodriguez " because -PRON- be hispanic . " -PRON- then keep -PRON- word and buy drink for other but not Rodriguez , the only hispanic Republican in the Assembly . 

 during the same event , Brooks make sexual comment to Duchow and Loudenbeck . 

 Rep. Mark Born ( R - Beaver Dam ) intervene during the episode , prompt Brooks to stop make the offensive remark . soon after , the three female lawmaker report -PRON- comment to Assembly Chief Clerk Patrick Fuller and ask Fuller to discuss the matter with Brooks . 

 Brooks later apologize , the three lawmaker say . a source say Brooks have maintain that -PRON- do not recall what -PRON- say to the woman . 

 during an interview Wednesday on WFAW - AM with host former democratic Rep. Andy Jorgensen , Assembly Speaker Pro Tem Tyler August ( R - Lake Geneva ) indicate no further action be need because the three female lawmaker be satisfied with the outcome . 

 " that be -PRON- main concern — be make sure that those three be comfortable with the outcome , " August say . 

 Brooks be elect assistant majority leader by -PRON- republican colleague in 2017 . also in leadership position be Rodriguez , who be caucus secretary , and Loudenbeck , who sit on the Legislature 's budget - writing committee . 

 Brooks say in a statement Tuesday -PRON- would make the " stupid " remark after -PRON- have be drink . 

 " -PRON- regret that -PRON- make some stupid comment while under the influence of alcohol after -PRON- caucus in the Dells , " -PRON- statement say . " -PRON- take full responsibility for -PRON- behavior and have apologize for -PRON- action . -PRON- be ready to move on from this incident and fully support and will adhere to the Assembly policy to maintain a safe workplace for legislator and staff . " 

 Brooks face Democrat Chris Rahlf of Cedarburg in November . 

 " sober or drunk , -PRON- be not ok to make these remark , " Rahlf say in a statement . " -PRON- be glad the woman involve be satisfied that the correct course of action be take . -PRON- must keep strive for a society where everyone be respect all the time . " 

 When ask for record relate to the harassment and -PRON- resolution , the chief clerk say there be none . 

 the Legislature come under scrutiny in the fall of 2017 over -PRON- policy to keep most detail and record relate to sexual harassment in the Legislature private . the question be raise amid a national wave of accusation of sexual misconduct against powerful man and lawmaker . 

 a Vos aide also do not respond to question about whether Brooks attend training offer to Assembly lawmaker and -PRON- staff intend to prevent harassing behavior .  

 Walker 's call for Brooks ' resignation come a day before President Donald Trump 's Supreme Court nominee Brett Kavanaugh will face a Senate panel along with a woman who be accuse Kavanaugh of sexually assault -PRON- in high school . 

 state Democrats on Wednesday call on Walker to oppose Kavanaugh 's confirmation in light of the allegation and Walker 's reaction to Brooks ' comment . -PRON- also say Walker be act hypocritical because -PRON- do not publicly rebuke Trump in 2016 when -PRON- say -PRON- have sexually assault woman . 

 " for Walker to call on Rep. Brooks to resign and be silent on Kavanaugh and be silent on President Trump when -PRON- be run ( for president ) be really kind of the height of hypocrisy , " Rep. Chris Taylor ( D - Madison ) say at a news conference . " -PRON- show that -PRON- act only in -PRON- own political interest . " 

 a spokesman do not respond to question about whether Walker still support Kavanaugh and Trump . 

 state school Superintendent Tony Evers , the Democrat challenging Walker in November , say in an interview -PRON- do not know enough about the situation to comment on whether Brooks should leave office .